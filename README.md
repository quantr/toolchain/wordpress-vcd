# Wordpress VCD introduction

Display waveform from a vcd file in wordpress, and it is our CPU testing workbench. Self-made tool for risc-v development, it is a vcd file dumper. With our own dumper, we can record the behavior of another risc-v, so we can cross check our risc-v correctless much more efficient.

# Test it and code it

1. cd public/app
2. npm run watch

# Author

Peter <peter@quantr.hk>, System Architect, Quantr Limited

![](https://www.quantr.foundation/wp-content/uploads/2021/10/Screenshot-2021-10-18-at-1.02.32-AM.png)

![](https://www.quantr.foundation/wp-content/uploads/2021/01/wordpress-vcd-20210127.png)

![](https://www.quantr.foundation/wp-content/uploads/2021/10/Screenshot-2021-10-28-at-7.01.34-PM.png)

