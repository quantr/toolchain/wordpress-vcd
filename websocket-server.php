<?php
require_once  'vendor/autoload.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;


        $w2 = new EvTimer(0, 1, function ($w) {
            echo "is called every second, is launched after 2 seconds\n";
            echo "iteration = ", Ev::iteration(), PHP_EOL;

            foreach ($this->clients as $client) {
                $client->send("timer ".Ev::iteration());
            }
        
            // Stop the watcher after 5 iterations
            // Ev::iteration() == 5 and $w->stop();
            // Stop the watcher if further calls cause more than 10 iterations
            // Ev::iteration() >= 10 and $w->stop();
        });
        
        echo "1\n";
        // $w2->again();
        // Ev::run(Ev::RUN_NOWAIT);
        // echo "2\n";
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";

        echo "end\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo "onMessage\n";
        $numRecv = count($this->clients) - 1;
        echo sprintf(
            'Connection %d sending message "%s" to %d other connection%s' . "\n",
            $from->resourceId,
            $msg,
            $numRecv,
            $numRecv == 1 ? '' : 's'
        );

        foreach ($this->clients as $client) {
            //if ($from !== $client) {
            // The sender is not the receiver, send to each client connected
            $client->send($msg);
            //}
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}


$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat()
        )
    ),
    9812
);

$server->run();
