<?
	global $wpdb;
?>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/jquery/jquery-3.3.1.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/popper/popper.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/js/bootstrap.min.js'></script>
<link href='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/css/bootstrap.min.css' type='text/css' rel='stylesheet' />
<link href='<?= plugin_dir_url(__FILE__) ?>/admin.css' type='text/css' rel='stylesheet' />

<?
	$users=array();
	$secret="121asdad21233";
?>
<script>
	function deleteUser(userID) {
		if (confirm('delete?')) {
			$.get('<?= plugin_dir_url(__FILE__) ?>/api.php?token=<?= $secret ?>&action=deleteUser&userID=' + userID + '&vcdToken=' + $('#vcdToken').val(), function(result) {
				alert('deleted');
				location.reload();
			});
		}
	}

	function addUser() {
		$.get('<?= plugin_dir_url(__FILE__) ?>/api.php?token=<?= $secret ?>&action=addUser&userID=' + $('#user').val() + '&vcd_token=' + $('#vcdToken').val(), function(result) {
			location.reload();
		});
	}
</script>
<table class="table">
	<thead>
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Token</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<? foreach (get_users(array('meta_key' => 'vcd_token')) as &$user){ ?>
		<tr>
			<td><?= $user->user_login ?></td>
			<td><?= $user->user_email ?></td>
			<td><?= get_user_meta($user->ID)['vcd_token'][0] ?></td>
			<td>
				<button type="button" class="btn btn-danger" onClick="deleteUser(<?= $user->ID ?>);">Delete</button>
			</td>
		</tr>
		<?
			array_push($users,$user->ID);
				//update_user_meta($user->ID, 'vcd_token', "peter123");
				// echo "<pre>";
				// echo (get_user_meta($user->ID)['vcd_token'][0]);
				// echo "</pre>";
			?>
		<? } ?>
	</tbody>
</table>
<form class="form-inline">
	<div class="form-group mb-2">
		<label for="user" class="sr-only">User</label>
		<select id="user">
			<? foreach (get_users() as &$user){ ?>
			<?
				if (in_array($user->ID, $users)){
					continue;
				}
			?>
			<option value="<?= $user->ID ?>"><?= $user->user_email ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group mx-sm-3 mb-2">
		<label for="token" class="sr-only">Token</label>
		<input class="form-control" id="vcdToken" />
	</div>
	<button type="button" class="btn btn-primary" onClick="addUser()">Add</button>
</form>

<!--
<pre>
	<? var_dump(get_users(array(fields => 'all_with_meta'))); ?>
</pre>
-->
