<?
function vcd_create_menu(){
    $first=plugin_dir_path(__FILE__) . 'mainSetting.php';
    add_menu_page(
        'WordPress VCD',
        'WordPress VCD',
        'manage_options',
        'vcd_mainMenuSlug',
        'sharepoint main menu',
        plugin_dir_url(__FILE__) . '../logo/wordpress vcd logo 16x16 white.png'
    );

    add_submenu_page(
        'vcd_mainMenuSlug',
        'User',
        'User',
        'manage_options',
        plugin_dir_path(__FILE__) . '/user.php',
        null
    );

    // add_submenu_page(
    //     'vcd_mainMenuSlug',
    //     'Product',
    //     'Product',
    //     'manage_options',
    //     plugin_dir_path(__FILE__) . '/product.php',
    //     null
    // );

	remove_submenu_page('vcd_mainMenuSlug', 'vcd_mainMenuSlug');
}

add_action('admin_menu', 'vcd_create_menu');
