<?php
/*
Plugin Name: WordPress VCD
Plugin URI: https://quantr.foundation
Description: Documentation engine for wordpress
Version: 1.2.0
Author: Peter
Support URI: https://quantr.foundation
Author URI: https://quantr.foundation
License: Quantr Open Source License

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

// include(get_home_path() . '/wp-includes/pluggable.php');
// include(plugin_dir_path(__FILE__) . 'public/shortcode.php');

global $wpdb;
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
include(plugin_dir_path(__FILE__) . 'public/shortcode.php');


include(plugin_dir_path(__FILE__) . 'admin/menu.php');

$charset_collate = $wpdb->get_charset_collate();

define('WORDPRESS_VCD_TABLE_PREFIX', 'wordpress_vcd_');

define('wordpress_vcd_vcdfile', WORDPRESS_VCD_TABLE_PREFIX . 'vcdFile');
$sql = "CREATE TABLE " . wordpress_vcd_vcdfile . " (
	id int(9) NOT NULL AUTO_INCREMENT,
	date datetime NULL,
	name varchar(100),
	username varchar(100),
	content text NOT NULL,
	yosys longtext NULL,
	topmoduleJson longtext NULL,
	scope_sequence text NULL,
	intputoutput text NULL,
	PRIMARY KEY (id)
) $charset_collate;";
dbDelta($sql);

function uploadYosys(WP_REST_Request $request)
{
	global $wpdb;
	// url is https://www.quantr.foundation/wp-json/wordpress-vcd/postResult
	// curl -i -X POST -H "Content-Type: multipart/form-data" -F "file1=@WhatsApp Image 2020-10-11 at 10.27.12 PM.jpeg" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadYosys/peter@quantr.hk/x1s2w3

	// echo "token=" . $request['token'] . "\n";
	// echo "email=" . $request['email'] . "\n";
	$user = get_user_by_email($request['email']);
	$vcd_token = get_user_meta($user->id)['vcd_token'][0];
	// echo "vcd_token=" . $vcd_token . "\n";
	if ($vcd_token != $request['token']) {
		echo "wrong token";
		return;
	}
	// echo "vcd name=" . $request['vcdName'] . "\n";
	$files = $request->get_file_params();
	if (!empty($files)) {
		$upload_overrides = array('test_form' => false);
		foreach ($files as $key => $file) {
			$wpdb->update(
				wordpress_vcd_vcdfile,
				array(
					'yosys' => base64_encode(file_get_contents($file['tmp_name'])),
				),
				array('username' => $request['email'], 'name' => $request['vcdName'])
			);
		}
	}
	echo "ok";
}

function uploadVCDFile(WP_REST_Request $request)
{
	global $wpdb;
	// url is https://www.quantr.foundation/wp-json/wordpress-vcd/postResult
	// curl -i -X POST -H "Content-Type: multipart/form-data" -F "file1=@WhatsApp Image 2020-10-11 at 10.27.12 PM.jpeg" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadYosys/peter@quantr.hk/x1s2w3

	// echo "token=" . $request['token'] . "\n";
	// echo "email=" . $request['email'] . "\n";
	$user = get_user_by_email($request['email']);
	$vcd_token = get_user_meta($user->id)['vcd_token'][0];
	// echo "vcd_token=" . $vcd_token . "\n";
	if ($vcd_token != $request['token']) {
		echo "wrong token";
		return;
	}
	// echo "vcd name=" . $request['vcdName'] . "\n";
	$files = $request->get_file_params();
	if (!empty($files)) {
		$upload_overrides = array('test_form' => false);
		foreach ($files as $key => $file) {
			$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';
			$output = shell_exec("$jdk_home/bin/java -jar " . plugin_dir_path(__FILE__) . "/jar/quantr-vcd-library-1.1.jar " . $file['tmp_name'] . " 2>&1");

			$r=$wpdb->update(
				wordpress_vcd_vcdfile,
				array(
					'content' => $output,
					"date" => current_time('mysql')
				),
				array('username' => $request['email'], 'name' => $request['vcdName'])
			);
		}
	}
	echo "ok";
}

function uploadTopmoduleJson(WP_REST_Request $request)
{
	global $wpdb;
	// url is https://www.quantr.foundation/wp-json/wordpress-vcd/postResult
	// curl -i -X POST -H "Content-Type: multipart/form-data" -F "file1=@WhatsApp Image 2020-10-11 at 10.27.12 PM.jpeg" https://www.quantr.foundation/wp-json/wordpress-vcd/uploadYosys/peter@quantr.hk/x1s2w3

	// echo "token=" . $request['token'] . "\n";
	// echo "email=" . $request['email'] . "\n";
	$user = get_user_by_email($request['email']);
	$vcd_token = get_user_meta($user->id)['vcd_token'][0];
	// echo "vcd_token=" . $vcd_token . "\n";
	if ($vcd_token != $request['token']) {
		echo "wrong token";
		return;
	}
	// echo "vcd name=" . $request['vcdName'] . "\n";
	$files = $request->get_file_params();
	if (!empty($files)) {
		// $upload_overrides = array('test_form' => false);
		foreach ($files as $key => $file) {
			$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';

			$wpdb->update(
				wordpress_vcd_vcdfile,
				array(
					'topmoduleJson' => file_get_contents($file['tmp_name']),
					"date" => current_time('mysql')
				),
				array('username' => $request['email'], 'name' => $request['vcdName'])
			);
		}
	}
	echo "ok";
}

function uploadScopeSequence(WP_REST_Request $request)
{
	global $wpdb;
	$user = get_user_by_email($request['email']);
	$vcd_token = get_user_meta($user->id)['vcd_token'][0];
	// echo "vcd_token=" . $vcd_token . "\n";
	if ($vcd_token != $request['token']) {
		echo "wrong token";
		return;
	}
	// echo "vcd name=" . $request['vcdName'] . "\n";
	$wpdb->update(
		wordpress_vcd_vcdfile,
		array(
			'scope_sequence' => $request['scope_sequence'],
		),
		array('username' => $request['email'], 'name' => $request['vcdName'])
	);
	echo "ok";
}

function inout(WP_REST_Request $request)
{
	global $wpdb;
	$user = get_user_by_email($request['email']);
	$vcd_token = get_user_meta($user->id)['vcd_token'][0];
	if ($vcd_token != $request['token']) {
		echo "wrong token";
		return;
	}
	$wpdb->update(
		wordpress_vcd_vcdfile,
		array(
			'intputoutput' => $request->get_body(),
		),
		array('username' => $request['email'], 'name' => $request['vcdName'])
	);
	echo "ok";
}

add_action('rest_api_init', function () {
	// https://www.quantr.hk/wp-json/wordpress-vcd/checkUsername/1
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/uploadYosys/(?P<email>[^/]+)/(?P<token>[^/]+)/(?P<vcdName>[^/]+)', array(
		'methods' => 'POST',
		'callback' => 'uploadYosys',
	));
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/uploadVCDFile/(?P<email>[^/]+)/(?P<token>[^/]+)/(?P<vcdName>[^/]+)', array(
		'methods' => 'POST',
		'callback' => 'uploadVCDFile',
	));
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/uploadTopmoduleJson/(?P<email>[^/]+)/(?P<token>[^/]+)/(?P<vcdName>[^/]+)', array(
		'methods' => 'POST',
		'callback' => 'uploadTopmoduleJson',
	));
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/uploadScopeSequence/(?P<email>[^/]+)/(?P<token>[^/]+)/(?P<vcdName>[^/]+)/(?P<scope_sequence>[^/]+)', array(
		'methods' => 'POST',
		'callback' => 'uploadScopeSequence',
	));
	register_rest_route(get_plugin_data(__FILE__)['TextDomain'], '/uploadInout/(?P<email>[^/]+)/(?P<token>[^/]+)/(?P<vcdName>[^/]+)', array(
		'methods' => 'POST',
		'callback' => 'inout',
	));
	// register_rest_route(get_plugin_data(__FILE__)['TextDomain'],'/addUser/(?P<firstname>[^/]+)/(?P<lastname>[^/]+)/(?P<company>[^/]+)/(?P<email>[^/]+)', array(
	// 	'methods' => 'GET',
	// 	'callback' => 'addUser',
	// ));
	// register_rest_route(get_plugin_data(__FILE__)['TextDomain'],'/checkUsername/(?P<email>[^/]+)', array(
	// 	'methods' => 'GET',
	// 	'callback' => 'checkUsername',
	// ));
	// register_rest_route(get_plugin_data(__FILE__)['TextDomain'],'/login', array(
	// 	'methods' => 'POST',
	// 	'callback' => 'login',
	// ));
	// register_rest_route(get_plugin_data(__FILE__)['TextDomain'],'/loginWithKey', array(
	// 	'methods' => 'POST',
	// 	'callback' => 'loginWithKey',
	// ));
	// register_rest_route(get_plugin_data(__FILE__)['TextDomain'],'/statistic/(?P<license>[^/]+)/(?P<type>[^/]+)/(?P<data>[^/]+)', array(
	// 	'methods' => 'GET',
	// 	'callback' => 'statistic',
	// ));
});
