<?
require $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
if ($_POST['action'] == 'uploadVCD') {
	$content=file_get_contents($_FILES['file']['tmp_name']);
	//echo $content;
	$jdk_home = '/home/gitlab-runner/Downloads/jdk-15';
	$output = shell_exec("$jdk_home/bin/java -jar ".plugin_dir_path(__FILE__)."../jar/quantr-vcd-library-1.1.jar ".$_FILES['file']['tmp_name']."  2>&1");
	echo $output;
	$_SESSION['last_vcd']=$output;
}else if ($_POST['action'] == 'saveScopes') {
	echo $_POST['scopes'];
	$_SESSION['last_scopes']=$_POST['scopes'];
}else if ($_POST['action'] == 'saveVcd') {
	$wpdb->insert(wordpress_vcd_vcdfile, array(
		"username" => wp_get_current_user()->user_email,
		"date" => current_time('mysql'),
		"name" => $_POST['name'],
		"username" => $_POST['username'],
		"content" => stripslashes($_POST['content'])
	));
	echo "ok";
}else if ($_POST['action'] == 'deleteVcd') {
	$wpdb->delete(wordpress_vcd_vcdfile, array('id' => $_POST['id']));
	echo "ok";
}else if ($_POST['action'] == 'loadSavedVCD') {
	// echo $wpdb->prepare("SELECT * FROM " . wordpress_vcd_vcdfile . " where username=%s order by name;", $_POST['username']);
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . wordpress_vcd_vcdfile . " where username=%s order by name;", $_POST['username']));
	echo json_encode($results);
}else if ($_POST['action'] == 'loadSavedVCDByName') {
	//echo $wpdb->prepare("SELECT * FROM " . wordpress_vcd_vcdfile . " where username=%s and name=%s order by name;", $_POST['username'], $_POST['name']);
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . wordpress_vcd_vcdfile . " where username=%s and name=%s order by name;", $_POST['username'], $_POST['name']));
	// echo $wpdb->prepare("SELECT * FROM " . wordpress_vcd_vcdfile . " where username=%s and name=%s order by name;", $_POST['username'], $_POST['name']);
	echo json_encode($results);
}else if ($_POST['action'] == 'saveSessionVCD') {
	$_SESSION['last_vcd']=stripslashes($_POST['content']);
	$_SESSION['last_yosys']=stripslashes($_POST['yosys']);
	$_SESSION['last_name']=stripslashes($_POST['name']);
}