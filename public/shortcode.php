<?
function wordpress_vcd_init()
{
    function wordpress_vcd_shortcode($atts = [], $content = null)
    {
		ob_start();
        include 'html.php';
        $content = ob_get_clean();
        return $content;
    }
    add_shortcode('wordpress-vcd', 'wordpress_vcd_shortcode');
}
add_action('init', 'wordpress_vcd_init');
