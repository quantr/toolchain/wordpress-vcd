global.__basedir = '/wp-content/plugins/quantr-docs/';

import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './Main.module.scss';

import * as $ from 'jquery';
import { FileDrop } from 'react-file-drop';
import myConfig from 'myConfig';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as solidIcons from "@fortawesome/free-solid-svg-icons";

export default class Main extends Component {

	scopesRow = [];
	wires = [];
	wireTH = [];
	wireTH_Top = [];
	addedWires = [];
	wireValues = [];
	referenceToWireName = [];
	referenceToWireSize = [];
	wireNameToReference = [];
	vcd = null;
	vcdString = null;
	isLoading = '';
	savedVCDButtons = [];
	scopes = [];
	name = null;
	scope_sequence = null;
	topModuleJson = null;
	myDiagram = null;

	colors = ['#DEFFFC', '#E2E4F6', '#fcd5ce', '#eddcd2', '#fff1e6'];

	instructionCache = [];

	disasmUrl = '/wp-content/plugins/AsmWeb/public/execQuantrDisassembler.php?arch=rv64&bytesStr=';
	yosys = null;
	inout = null

	getColor(x) {
		return this.colors[x % this.colors.length];
	}

	componentDidMount() {
		console.log('myConfig.last_name', myConfig.last_name);
		if (myConfig.last_name != '') {
			this.init(myConfig.last_name);
		}

		this.loadSavedVCDButtons();
	}

	init(vcdName) {
		var fd = new FormData();
		fd.append("action", "loadSavedVCDByName");
		fd.append("name", vcdName);
		fd.append("username", myConfig.user_email);
		var that = this;
		$.ajax({
			url: myConfig.plugin_dir_url + '/api.php',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			success: function (response) {
				$('#savedVCDDate').html(JSON.parse(response)[0]['date']);
				that.scope_sequence = JSON.parse(response)[0]['scope_sequence'];
				that.loadInOut(JSON.parse(response)[0]['intputoutput']);
				that.loadVCD(JSON.parse(response)[0]['content']);
				that.loadYosys(JSON.parse(response)[0]['yosys']);
				that.loadTopModule(JSON.parse(response)[0]['topmoduleJson']);
				$('#vcdDiv').html(that.vcd);
			},
		});
	}

	loadGoJS = (json) => {
		if (this.myDiagram != null) {
			this.myDiagram.div = null;
		}

		const go = require("gojs");
		const $ = go.GraphObject.make;

		this.myDiagram =
			$(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
				{
					allowDelete: false,
					allowCopy: false,
					layout: $(go.ForceDirectedLayout),
					"undoManager.isEnabled": true
				});

		var colors = {
			'red': '#be4b15',
			'green': '#52ce60',
			'blue': '#6ea5f8',
			'lightred': '#fd8852',
			'lightblue': '#afd4fe',
			'lightgreen': '#b9e986',
			'pink': '#faadc1',
			'purple': '#d689ff',
			'orange': '#fdb400',
		}

		// define the Node template, representing an entity
		this.myDiagram.nodeTemplate =
			$(go.Node, "Table",
				{
					locationObjectName: "BODY",
					locationSpot: go.Spot.Center,
					selectionObjectName: "BODY",
					// isLayoutPositioned: false
				},
				new go.Binding("location", "loc", go.Point.parse),
				// the body
				$(go.Panel, "Auto",
					{
						row: 1, column: 1, name: "BODY",
						stretch: go.GraphObject.Fill
					},
					$(go.Shape, "Rectangle",
						{
							fill: "#dbf6cb", stroke: null, strokeWidth: 0,
							minSize: new go.Size(60, 60)
						}),
					$(go.TextBlock,
						{
							margin: 10, textAlign: "center", font: "bold 24px Segoe UI,sans-serif", stroke: "#484848"
						},
						new go.Binding("text", "name").makeTwoWay())
				),  // end Auto Panel body

				// the Panel holding the left port elements, which are themselves Panels,
				// created for each item in the itemArray, bound to data.leftArray
				$(go.Panel, "Vertical",
					new go.Binding("itemArray", "leftArray"),
					{
						row: 1, column: 0,
						itemTemplate:
							$(go.Panel, "Horizontal",
								{
									_side: "left",  // internal property to make it easier to tell which side it's on
									fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
									fromLinkable: false, toLinkable: false, cursor: "pointer",
								},
								new go.Binding("portId", "portId"),
								{ margin: new go.Margin(3, 0) },  // some space between ports
								$(go.TextBlock,
									{
										// margin: new go.Margin(10, 0),
										// textAlign: "center",
										font: "bold 14px Segoe UI,sans-serif",
										stroke: "#484848"
									},
									new go.Binding("text", "portName").makeTwoWay()
								),
								$(go.Shape, "Rectangle",
									{
										stroke: null, strokeWidth: 0,
										desiredSize: new go.Size(8, 8),
										margin: new go.Margin(0, 0, 0, 5)
									},
									new go.Binding("fill", "portColor"),
								),
							)  // end itemTemplate
					}
				),  // end Vertical Panel

				// the Panel holding the right port elements, which are themselves Panels,
				// created for each item in the itemArray, bound to data.rightArray
				$(go.Panel, "Vertical",
					new go.Binding("itemArray", "rightArray"),
					{
						row: 1, column: 2,
						itemTemplate:
							$(go.Panel, "Horizontal",
								{
									_side: "right",
									fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
									fromLinkable: false, toLinkable: false, cursor: "pointer",
								},
								new go.Binding("portId", "portId"),
								{ margin: new go.Margin(3, 0) },  // some space between ports
								$(go.Shape, "Rectangle",
									{
										stroke: null, strokeWidth: 0,
										desiredSize: new go.Size(8, 8),
										margin: new go.Margin(0, 5, 0, 0)
									},
									new go.Binding("fill", "portColor"),
								),
								$(go.TextBlock,
									{
										// margin: new go.Margin(10, 0),
										// textAlign: "center",
										font: "bold 14px Segoe UI,sans-serif",
										stroke: "#484848"
									},
									new go.Binding("text", "portName").makeTwoWay()
								),
							)  // end itemTemplate
					}
				),  // end Vertical Panel
			);  // end Node

		// define the Link template, representing a relationship
		this.myDiagram.linkTemplate =
			$(go.Link,  // the whole link panel
				{
					selectionAdorned: true,
					layerName: "Foreground",
					reshapable: true,
					routing: go.Link.AvoidsNodes,
					corner: 10,
					curve: go.Link.JumpGap
				},
				$(go.Shape,  // the link shape
					{ stroke: "#aaa", strokeWidth: 2.5 }),
				// $(go.Shape,  // the arrowhead
				// { toArrow: "standard", stroke: "#aaa", strokeWidth: 2.5 }),
				$(go.Panel, "Auto",
					$(go.Shape,  // the label background, which becomes transparent around the edges
						{
							//fill: $(go.Brush, "Radial", { 0: "rgb(255, 255, 255)", 1: "rgba(255, 255, 255)" }),
							fill: '#e0cfeb',
							stroke: null
						}),
					$(go.TextBlock,  // the label text
						{
							textAlign: "center",
							font: "10pt helvetica, arial, sans-serif",
							stroke: "#000",
							margin: 10
						},
						new go.Binding("text", "midText"))
				),
				// $(go.TextBlock,  // the "from" label
				// 	{
				// 		textAlign: "center",
				// 		font: "bold 14px sans-serif",
				// 		stroke: "#1967B3",
				// 		segmentIndex: 0,
				// 		segmentOffset: new go.Point(NaN, NaN),
				// 		segmentOrientation: go.Link.OrientUpright
				// 	},
				// 	new go.Binding("text", "fromText")),
				// $(go.TextBlock,  // the "to" label
				// 	{
				// 		textAlign: "center",
				// 		font: "bold 14px sans-serif",
				// 		stroke: "#1967B3",
				// 		segmentIndex: -1,
				// 		segmentOffset: new go.Point(NaN, NaN),
				// 		segmentOrientation: go.Link.OrientUpright
				// 	},
				// 	new go.Binding("text", "toText"))
			);

		// create the model for the E-R diagram
		let nodeDataArray = [];
		console.log(json.modules);
		let x = 1;
		let nodeNo = 0;
		for (const key in json.modules) {
			let module = json.modules[key];
			let leftPorts = [];
			for (const key2 in module.inputs) {
				let input = module.inputs[key2];
				leftPorts.push(
					{ portColor: '#66d6d1', portId: module.name + "_" + input.name, portName: input.name }
				);
				x++;
			}

			let rightPorts = [];
			for (const key2 in module.outputs) {
				let output = module.outputs[key2];
				rightPorts.push(
					{ portColor: '#66d6d1', portId: module.name + "_" + output.name, portName: output.name }
				);
				x++;
			}

			nodeDataArray.push(
				{
					name: module.name,
					key: module.name,
					// items: ports,
					"leftArray": leftPorts,
					"rightArray": rightPorts,
					loc: (nodeNo * 400) + " 300"
				}
			);
			nodeNo++;
		}

		console.log(json.wires);
		let linkDataArray = [];
		for (const key in json.wires) {
			let wire = json.wires[key];
			linkDataArray.push(
				{
					from: wire.port1Module.name,
					to: wire.port2Module.name,
					fromPort: wire.port1Module.name + "_" + wire.port1.name,
					toPort: wire.port2Module.name + "_" + wire.port2.name,
					// fromText: "NN",
					// toText: "asd",
					midText: wire.name + '\n' + wire.port1.range
				},
			)
		}
		// var linkDataArray = [
		// 	{ from: "pc_reg", to: "rom", fromPort: "n1", toPort: "n5", fromText: "NN", toText: "asd", midText: 'fuck' },
		// ];
		this.myDiagram.model = $(go.GraphLinksModel,
			{
				copiesArrays: true,
				copiesArrayObjects: true,
				linkFromPortIdProperty: "fromPort",
				linkToPortIdProperty: "toPort",
				nodeDataArray: nodeDataArray,
				linkDataArray: linkDataArray
			});
		this.myDiagram.layout = $(
			go.TreeLayout,
			{
				angle: 0,
				arrangement: go.TreeLayout.ArrangementHorizontal,
				//  treeStyle: go.TreeLayout.StyleLastParents,
				layerSpacing: 400,
			});
		this.forceUpdate();
	}

	loadVCD(response) {
		try {
			this.vcdString = response;
			this.vcd = JSON.parse(response);
			this.scopesRow = [];
			this.wires = [];

			this.loadScopeCheckbox(this.vcd.scope, 0);
			this.forceUpdate();
			if (myConfig.last_scopes != '') {
				this.scopes = myConfig.last_scopes.split(',');
				this.initVariables(this.scopes);
			}
			this.loadData();
		} catch (ex) {
			console.log(ex.stack);
		}
	}

	loadYosys(response) {
		try {
			this.yosys = atob(response);
			this.forceUpdate();
		} catch (ex) {
			console.log(ex.stack);
		}
	}

	loadTopModule(response) {
		this.topModuleJson = JSON.parse(response);
		this.loadGoJS(this.topModuleJson);
	}

	loadInOut(response) {
		try {
			this.inout = JSON.parse(response);
			// console.log(this.inout);
			this.forceUpdate();
		} catch (ex) {
			console.log(ex.stack);
		}
	}

	dropedFiles = (files, event) => {
		var fd = new FormData();

		fd.append("action", "uploadVCD");
		for (let i = 0; i < files.length; i++) {
			fd.append("file", files[i]);
		}

		var that = this;
		$.ajax({
			url: myConfig.plugin_dir_url + '/api.php',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			success: function (response) {
				$('#vcdDiv').html(response);
				that.loadVCD(response);
			},
		});
	}

	scopeChange = (e) => {
		if (e.target.checked) {
			this.scopes.push(e.target.value);
		} else {
			this.scopes = this.scopes.filter(obj => obj !== e.target.value);
		}
		// let scopes = [];
		// $('#scopeTable input[type="checkbox"]').each(function () {
		//  if ($(this).is(":checked")) {
		//      scopes.push($(this).val());
		//  }
		// });
		this.initVariables(this.scopes);
		this.saveScopes(this.scopes);
		this.loadData();
	}

	updateScope = () => {
		this.scopes = [];
		let that = this;
		$('#scopeTable input[type="checkbox"]').each(function () {
			if ($(this).is(":checked")) {
				that.scopes.push($(this).val());
			}
		});
		this.initVariables(this.scopes);
		this.saveScopes(this.scopes);
		this.loadData();
	}

	initVariables = (scopes) => {
		/*
			this function init all the variables
		*/
		this.wireTH = [];
		this.wireTH_Top = [];
		this.addedWires = [];
		this.wireValues = [];
		this.referenceToWireName = [];
		this.referenceToWireSize = [];
		this.wireNameToReference = [];
		this.wireTH_Top.push(<th style={{ backgroundColor: 'white' }}></th>);
		this.wireTH.push(<th className="rotate" style={{ backgroundColor: 'white' }}><div></div></th>);

		let tempX = 0;
		for (let index = 0; index < scopes.length; index++) {
			let scopeName = scopes[index];
			let temp = this.wires[scopeName];
			if (temp == null) {
				break;
			}

			let inputs;
			let outputs;
			if (this.inout != null) {
				for (let i = 0; i < this.inout.length; i++) {
					// console.log('this.inout[i].moduleName=' + this.inout[i].moduleName);
					if (scopeName.startsWith(this.inout[i].moduleName)) {
						inputs = this.inout[i].inputs;
						outputs = this.inout[i].outputs;
					}
				}
			}
			// console.log(inputs);
			// console.log(outputs);

			this.wireTH_Top.push(<th colSpan={temp.length} colSpanOri={temp.length} style={{ textAlign: 'center' }}>{scopeName}</th>);
			temp.sort(function (a, b) {
				if (inputs != null && inputs.indexOf(a.name) > -1 && inputs.indexOf(b.name) > -1) {
					let aa = (-1 * (inputs.length - inputs.indexOf(a.name)));
					let bb = (-1 * (inputs.length - inputs.indexOf(b.name)));
					return aa - bb;
				} else if (inputs != null && inputs.indexOf(a.name) > -1) {
					return -1;
				} else if (outputs != null && outputs.indexOf(a.name) > -1 && outputs.indexOf(b.name) > -1) {
					let aa = (-1 * (outputs.length - outputs.indexOf(a.name)));
					let bb = (-1 * (outputs.length - outputs.indexOf(b.name)));
					return aa - bb;
				} else if (outputs != null && outputs.indexOf(a.name) > -1) {
					return 1;
				} else if (outputs != null && outputs.indexOf(b.name) > -1) {
					return -1;
				} else {
					return 1;
				}
			});
			let temp2;
			if (this.inout != null) {
				for (let x = 0; x < this.inout.length; x++) {
					if (scopeName.startsWith(this.inout[x].moduleName) && (temp2 == null || this.inout[x].moduleName.length > temp2.moduleName.length)) {
						temp2 = this.inout[x];
						break;
					}
				}
			}
			for (let i = 0; i < temp.length; i++) {
				let wireName = temp[i].name;
				let wireSize = temp[i].size;
				let type;
				if (temp2 != null && temp2.inputs != null && temp2.inputs.indexOf(wireName) > -1) {
					type = <span className="inputCircle">I</span>
				} else if (temp2 != null && temp2.outputs != null && temp2.outputs.indexOf(wireName) > -1) {
					type = <span className="outputCircle">O</span>
				}
				let abc = tempX + 2;
				this.wireTH.push(<th className="rotate" onClick={() => { this.selectCol(abc); }} style={{ zIndex: 1000 - tempX }}><div>{wireName} {type}</div></th>);
				this.addedWires.push(temp[i]);
				this.wireValues[wireName] = 0;
				this.referenceToWireName[temp[i].reference] = wireName;
				this.referenceToWireSize[temp[i].reference] = wireSize;
				this.wireNameToReference[wireName] = temp[i].reference;
				tempX++;
			}
		}
	}

	selectCol(colIndex) {
		$('#wireTable td:nth-child(' + colIndex + ')').toggleClass('selectedTDVertical');
	}

	saveScopes(scopes) {
		var fd = new FormData();

		fd.append("action", "saveScopes");
		fd.append("scopes", scopes);

		var that = this;
		$.ajax({
			url: myConfig.plugin_dir_url + '/api.php',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			success: function (response) {
			},
		});
	}

	loadScopeCheckbox(scope, indent) {
		let color;
		let number;
		let defaultChecked;
		if (this.scope_sequence != null) {
			// console.log(this.scope_sequence.split(","));
			// console.log(scope.name + ' = ' + this.scope_sequence.split(",").indexOf(scope.name));
			color = this.scope_sequence.split(",").indexOf(scope.name) > -1 ? "black" : "";
			number = this.scope_sequence.split(",").indexOf(scope.name);
		}
		if (myConfig.last_scopes.split(',').includes(scope.name)) {
			defaultChecked = "checked";
		}
		// if (myConfig.last_scopes.split(',').includes(scope.name)) {
		this.scopesRow.push(
			<tr>
				<td style={{ color: color }} >
					<span style={{ width: number > -1 ? '10px' : indent * 10 + 'px', display: 'inline-block' }}>&nbsp;</span><input type="checkbox" id={scope.name} value={scope.name} onChange={this.scopeChange} defaultChecked={defaultChecked} /> <label htmlFor={scope.name} style={{ cursor: 'pointer' }}>{scope.name}</label>
					{number > -1 &&
						<span style={{ color: 'white', backgroundColor: '#cdb4db', borderRadius: '50%', marginLeft: indent * 10 + 'px', float: 'left', width: '25px', display: 'inline-block', textAlign: 'center' }}>{number}</span>
					}
				</td>
			</tr>
		);
		this.wires[scope.name] = scope.wires;

		let arr = scope.scopes;
		let that = this;
		arr.sort(function (a, b) {
			if (that.scope_sequence == null) {
				return 0;
			}
			let aIndex = that.scope_sequence.split(",").indexOf(a.name);
			let bIndex = that.scope_sequence.split(",").indexOf(b.name);
			if (aIndex < bIndex) return -1;
			if (aIndex > bIndex) return 1;
			return 0;
		});
		for (let x = 0; x < arr.length; x++) {
			this.loadScopeCheckbox(arr[x], indent + 1);
		}
	}

	loadData = () => {
		console.log('loadData', this.vcd);
		if (this.vcd == null) {
			return;
		}
		this.showLoading(true);
		let data = this.vcd.data;
		this.dataTR = [];
		this.forceUpdate();
		// let bingo = 0;
		// let temp = [];

		// console.log(data);
		let rowsData = [];
		for (let i = 0; i < data.length; i++) {
			if (data[i].left[0] == '#') {
				rowsData.push({ time: data[i].left, data: [] });
			} else {
				rowsData[rowsData.length - 1].data.push(data[i]);
			}
		}
		console.log('rowsData', rowsData);
		// console.log(this.dataTR);

		for (let i = 0; i < rowsData.length && i < $('#noOfRow').val(); i++) {
			for (let x = 0; x < rowsData[i].data.length; x++) {
				let value = rowsData[i].data[x];
				let reference;
				let val;
				if (value.right == null) {
					reference = value.left.slice(1);
					val = value.left.slice(0, 1);
				} else {
					reference = value.right;
					val = value.left;
				}

				this.wireValues[reference] = val;
			}

			let columns = [];
			columns.push(<td>{rowsData[i].time}</td>);
			for (let x = 0; x < this.addedWires.length; x++) {
				// console.log(this.wireValues);
				let wireValue = this.wireValues[this.wireNameToReference[this.addedWires[x].name]];
				let wireSize = this.referenceToWireSize[this.wireNameToReference[this.addedWires[x].name]];
				// console.log('> '+this.addedWires[x].name+'='+wireValue);
				if (wireValue[0] == 'b') {
					if ($('#fullWireWidth').prop('checked')) {
						wireValue = parseInt(wireValue.slice(1), 2).toString(16).padStart(wireSize / 4, '0');
					} else {
						wireValue = parseInt(wireValue.slice(1), 2).toString(16);
					}
					// console.log('< '+this.addedWires[x].name+'='+wireValue);
				}
				columns.push(<td custom-attribute={`${i}_${x}`} wireName={this.addedWires[x].name} style={{ textAlign: 'center', borderRight: '1px solid #aaa', whiteSpace: 'nowrap', display: '' }}>{wireValue}</td>);
			}
			this.dataTR.push(<tr>{columns}</tr>);
		}
		// console.log(this.dataTR);
		this.forceUpdate(() => {
			this.showRiseEdgeOnly($('#riseEdgeOnly').prop('checked'));
			this.showInputAndOutputOnly($('#showOnlyInOutCheckbox').prop('checked'));
			this.setTableColor();
			this.showLoading(false);
			this.disassemble();
		});
	}

	disassemble = () => {
		let index;
		let that = this;
		$.each($('#wireTable thead tr:nth-child(2) th div'), function (key, value) {
			if (value.innerHTML.indexOf('inst') > -1) {
				index = key;
				return false;
			}
		});
		if (index === undefined) {
			return;
		}
		$('.instruction').remove();
		$.each($('#wireTable tbody td:nth-child(' + (index + 1) + ')'), function (key, value) {
			let v = value.innerHTML.trim().padStart(8, '0');
			// console.log('A=' + value.innerHTML);
			// console.log('v=' + v);
			let byteCodes = '';
			for (let x = 6; x >= 0; x -= 2) {
				byteCodes += v.substr(x, 2);
			}
			// console.log('byteCodes=' + byteCodes);
			if (that.instructionCache[byteCodes] == undefined) {
				let data = $.ajax({
					type: "GET",
					url: that.disasmUrl + byteCodes,
					cache: false,
					async: false
				}).responseText;

				console.log(data);
				if (data != null) {
					that.instructionCache[byteCodes] = data;
					let instruction = data.split('\t')[3];
					$(value).html($(value).html() + ' <span class="instruction">' + instruction + '</span>');
					$(value).css('text-align', 'left');
				}
			} else {
				let instruction = that.instructionCache[byteCodes].split('\t')[3];
				$(value).html($(value).html() + ' <span class="instruction">' + instruction + '</span>');
				$(value).css('text-align', 'left');
			}
			// console.log(that.instructionCache);
		});
	}

	RGBToHex(rgb) {
		// Choose correct separator
		let sep = rgb.indexOf(",") > -1 ? "," : " ";
		// Turn "rgb(r,g,b)" into [r,g,b]
		rgb = rgb.substr(rgb.indexOf("(") + 1).split(")")[0].split(sep);

		let r = (+rgb[0]).toString(16),
			g = (+rgb[1]).toString(16),
			b = (+rgb[2]).toString(16);

		if (r.length == 1)
			r = "0" + r;
		if (g.length == 1)
			g = "0" + g;
		if (b.length == 1)
			b = "0" + b;

		return "#" + r + g + b;
	}


	shadeColor(color, percent) {
		var R = parseInt(color.substring(1, 3), 16);
		var G = parseInt(color.substring(3, 5), 16);
		var B = parseInt(color.substring(5, 7), 16);

		R = parseInt(R * (100 + percent) / 100);
		G = parseInt(G * (100 + percent) / 100);
		B = parseInt(B * (100 + percent) / 100);

		R = (R < 255) ? R : 255;
		G = (G < 255) ? G : 255;
		B = (B < 255) ? B : 255;

		var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
		var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
		var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

		return "#" + RR + GG + BB;
	}

	setTableColor() {
		let colNo = 2;
		for (let x = 1; x < $('#wireTable thead th').length; x++) {
			$('#wireTable thead tr:nth-child(1) th:nth-child(' + (x + 1) + ')').css('background-color', this.getColor(x - 1));
			let colspan = $($('#wireTable thead th')[x]).attr('colSpanOri');
			for (let z = 0; z < colspan; z++) {
				$('#wireTable thead tr:nth-child(2) th:nth-child(' + colNo + ')').css('background-color', this.getColor(x - 1));
				$('#wireTable thead:nth-child(2) th:nth-child(' + colNo + ') div').css('background-color', this.getColor(x - 1));
				$('#wireTable tbody td:nth-child(' + colNo + ')').css('background-color', this.getColor(x - 1));
				colNo++;
			}
		}

		let that = this;
		$('#wireTable tbody tr:even td:not(:first-child)').each(function () {
			let colorHex = that.RGBToHex($(this).css('background-color'));
			$(this).css('background-color', that.shadeColor(colorHex, 7));
		});

		$('#wireTable').find('tr').hover(function () {
			let rowNo = $(this).index() + 1;
			$('#wireTable tbody tr td').removeClass('hoverTR');
			$('#wireTable tbody tr:nth-child(' + rowNo + ') td').addClass('hoverTR');
		})

		$("#wireTable tr").off("click");
		$("#wireTable tr").click(function () {
			console.log($(this));
			console.log($(this).find('td'));
			$(this).find('td').toggleClass("selectedTD");
		});

		$("#wireTable td").off("click");
		$("#wireTable td").click(function () {
			let wireName = $(this).attr('wireName');
		});
	}

	showRiseEdgeOnly(b) {
		if (b) {
			let th = $('#wireTable thead tr:nth-child(2) th div');
			let clkColNo;
			for (let x = 0; x < th.length; x++) {
				if ($(th[x]).html().indexOf('clk') > -1) {
					clkColNo = x + 1;
					break;
				}
			}
			if (clkColNo != undefined) {
				let td = $('#wireTable tbody td:nth-child(' + clkColNo + ')');
				for (let x = td.length - 1; x >= 0; x--) {
					if ($(td[x]).html() == '0') {
						$('#wireTable tbody tr:nth-child(' + (x + 1) + ')').css('display', 'none');
					}
				}
			}
		} else {
			$('#wireTable tbody tr').css('display', '');
		}
	}

	showInputAndOutputOnly(b) {
		let th = $('#wireTable thead tr:nth-child(2) th');
		let topTH = $('#wireTable thead tr:nth-child(1) th');
		if (b) {
			let bigColNo = 1;
			let bigColIndex = 1;
			// console.log(topTH);
			let noOfHidedCol;
			for (let x = 1; x < th.length; x++) {
				if (x == bigColIndex) {
					if (x > 1) {
						// console.log('noOfHidedCol=' + noOfHidedCol);
						// console.log('new=' + (parseInt($(topTH[bigColNo - 1]).attr('colspanOri')) - noOfHidedCol));
						// console.log('bigColNo=' + bigColNo);
						$(topTH[bigColNo - 1]).attr('colspan', parseInt($(topTH[bigColNo - 1]).attr('colspanOri')) - noOfHidedCol);
					}
					let colspanOri = parseInt($(topTH[bigColNo]).attr('colspanOri'));
					// console.log(colspan);
					bigColIndex += colspanOri;
					bigColNo++;
					noOfHidedCol = 0
				}
				if ($(th[x]).html().indexOf('inputCircle') == -1 && $(th[x]).html().indexOf('outputCircle') == -1) {
					$('#wireTable tr:nth-child(2) th:nth-child(' + (x + 1) + ')').css('display', 'none');
					$('#wireTable tr:nth-child(3) th:nth-child(' + (x + 1) + ')').css('display', 'none');
					$('#wireTable td:nth-child(' + (x + 1) + ')').css('display', 'none');
					// debugger;
					// $('#wireTable td:nth-child(' + (x + 1) + ')').css('color', 'red');
					// $('#wireTable td:nth-child(' + (x + 1) + ')').html('F' + (x + 1) + ',' + x);
					// $(th[x]).css('color', 'red');
					noOfHidedCol++;
				} else {
					$('#wireTable tr:nth-child(2) th:nth-child(' + (x + 1) + ')').css('display', '');
					$('#wireTable td:nth-child(' + (x + 1) + ')').css('display', '');
				}
			}
		} else {
			$('#wireTable tr:nth-child(2) th').css('display', '');
			$('#wireTable tr:nth-child(3) th').css('display', '');
			$('#wireTable td').css('display', '');
			for (let x = 1; x < th.length; x++) {
				$(topTH[x]).attr('colspan', $(topTH[x]).attr('colspanOri'));
			}
		}
	}

	highlightRiseEdgeOnly(b) {
		this.showLoading(true);
		if (b) {
			let colNo = 0;
			let topTH = $('#wireTable thead tr:nth-child(1) th');
			for (let x = 1; x < topTH.length; x++) {
				let colspan = parseInt($(topTH[x]).attr('colspan'));

				// find clk
				let clkColNo = -1;
				let th = $('#wireTable thead tr:nth-child(2) th div');
				for (let z = 0; z < colspan; z++) {
					if ($(th[colNo + z]).html().indexOf('clk') > -1) {
						clkColNo = colNo + z + 1;
						break;
					}
				}
				// end find clk

				if (clkColNo != -1) {
					console.log('clkColNo=' + clkColNo);
					let tr = $('#wireTable tbody tr');
					for (let y = 0; y < tr.length; y++) {
						let td = $('#wireTable tbody tr:nth-child(' + (y + 1) + ') td:nth-child(' + clkColNo + ')');
						if ($(td).html() == 1) {
							let temp = $('#wireTable tbody tr:nth-child(' + (y + 1) + ') td').slice(colNo + 1, colNo + +1 + colspan);
							$(temp).css('border-left', '2px solid #777');

						}
					}
				}

				colNo += parseInt(colspan);
			}
		} else {
			$('#wireTable tbody td').css('border-left', '');
		}
		this.showLoading(false);
	}

	showLoading(b) {
		if (b) {
			this.isLoading = '';
		} else {
			this.isLoading = 'none';
		}
		this.forceUpdate();
	}

	saveVCD = () => {
		console.log(JSON.stringify(this.vcd));
		let vcdName = prompt("VCD name ?");
		if (vcdName) {
			var fd = new FormData();

			fd.append("action", "saveVcd");
			fd.append("name", vcdName);
			fd.append("username", myConfig.user_email);
			fd.append("content", JSON.stringify(this.vcd));

			var that = this;
			$.ajax({
				url: myConfig.plugin_dir_url + '/api.php',
				type: 'post',
				data: fd,
				contentType: false,
				processData: false,
				success: function (response) {
					that.loadSavedVCDButtons();
				},
			});
		}
	}

	loadSavedVCDButtons() {
		console.log('loadSavedVCDButtons');
		this.savedVCDButtons = [];
		var fd = new FormData();
		fd.append("action", "loadSavedVCD");
		fd.append("username", myConfig.user_email);
		let that = this;
		$.ajax({
			url: myConfig.plugin_dir_url + '/api.php',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			success: function (response) {
				// console.log('loadSavedVCDButtons response', response);
				let json = JSON.parse(response);
				for (let x = 0; x < json.length; x++) {
					let item = json[x];
					// console.log('item.content', item.content);
					that.savedVCDButtons.push(<div style={{ whiteSpace: 'nowrap' }}>
						<button className="btn btn-secondary" style={{ marginBottom: '5px', marginRight: '0px', borderTopRightRadius: '0px', borderBottomRightRadius: '0px' }} onClick={() => { console.log(item); myConfig.last_scopes = ''; that.name = item.name; that.scope_sequence = item.scope_sequence; $('#noneButton').trigger('click'); that.init(item.name); $('#vcdDiv').html(item.content); that.saveSessionVCD(item.content, item.yosys, item.name); }}>{item.name}</button>
						<button className="btn btn-secondary" style={{ marginBottom: '5px', marginRight: '5px', borderTopLeftRadius: '0px', borderBottomLeftRadius: '0px', color: '#aaa' }} onClick={() => { myConfig.last_scopes = ''; $('#noneButton').trigger('click'); that.deleteVcd(item); }}><sup>X</sup></button>
					</div>);
					// that.savedVCDButtons.push(<br />);
				}
				console.log('that.savedVCDButtons', that.savedVCDButtons);
				that.forceUpdate();
			},
		});
	}

	saveSessionVCD(content, yosys, name) {
		var fd = new FormData();
		fd.append("action", "saveSessionVCD");
		fd.append("content", content);
		fd.append("yosys", yosys);
		fd.append("name", name);
		var that = this;
		$.ajax({
			url: myConfig.plugin_dir_url + '/api.php',
			type: 'post',
			data: fd,
			contentType: false,
			processData: false,
			success: function (response) {
			},
		});
	}

	deleteVcd(item) {
		if (confirm("Delete?")) {
			var fd = new FormData();
			fd.append("action", "deleteVcd");
			fd.append("id", item.id);
			var that = this;
			$.ajax({
				url: myConfig.plugin_dir_url + '/api.php',
				type: 'post',
				data: fd,
				contentType: false,
				processData: false,
				success: function (response) {
					that.loadSavedVCDButtons();
				},
			});
		}
	}

	onFileInputChange = (event) => {
		const { files } = event.target;
		this.dropedFiles(files, null);
	}
	onTargetClick = () => {
		this.refs['fileInputRef'].click();
	}

	copyToken = () => {
		var r = document.createRange();
		r.selectNode(document.getElementById('token'));
		window.getSelection().removeAllRanges();
		window.getSelection().addRange(r);
		document.execCommand('copy');
		window.getSelection().removeAllRanges();
		// $('#token').css('border', '3px dashed black');
		$('#token').css('color', '#ccc');
	}

	preSelectScopes = () => {
		if (this.scope_sequence == null) {
			return;
		}
		this.scopes = this.scope_sequence.split(",");
		for (let x = 0; x < this.scopes.length; x++) {
			$('#scopeTable #' + this.scopes[x]).prop('checked', true);
		}
		this.initVariables(this.scopes);
		this.saveScopes(this.scopes);
		this.loadData();
	}

	render() {
		return (
			<div className="main">
				<div className='container-fluid' style={{ height: 'calc(100vh - 180px)', overflow: 'auto' }}>
					<div className='row'>
						<div className='col-lg-2 col-md-2 col-sm-12 mt-1'>
							<input
								onChange={this.onFileInputChange}
								ref="fileInputRef"
								type="file"
								className="hidden"
								style={{ display: 'none' }}
							/>
							<div onClick={this.copyToken} id="token" style={{ width: '100%', border: '3px dashed #ccc', marginLeft: '5px', textAlign: 'center', cursor: 'pointer' }}>
								Token={myConfig.vcd_token}
							</div>
							<FileDrop
								onDrop={this.dropedFiles}
								onTargetClick={this.onTargetClick}
							>
								Drop vcd file here
							</FileDrop>
							<table id="scopeTable">
								<tbody>
									{this.scopesRow}
								</tbody>
							</table>
							<button className="btn btn-primary" onClick={() => { $('#scopeTable input').prop('checked', true); this.updateScope(); }}>All</button>&nbsp;
							<button className="btn btn-primary" onClick={this.preSelectScopes}>Pre</button>&nbsp;
							<button className="btn btn-primary" id="noneButton" onClick={() => { $('#wireTable td').removeClass('selectedTD'); $('#scopeTable input').prop('checked', false); this.updateScope(); }}>None</button>
							<br />
							<select id="noOfRow" className="form-control" onChange={this.loadData} style={{ marginTop: '5px' }}>
								<option>50</option>
								<option>100</option>
								<option>200</option>
								<option>500</option>
								<option>1000</option>
							</select>
							<br />
							<input type="checkbox" id="fullWireWidth" onClick={this.loadData} /> <label htmlFor="fullWireWidth" style={{ cursor: 'pointer' }}>Full Wire Width</label>
							<br />
							<input type="checkbox" id="riseEdgeOnly" defaultChecked onClick={() => { this.showRiseEdgeOnly($('#riseEdgeOnly').prop('checked')); }} /> <label htmlFor="riseEdgeOnly" style={{ cursor: 'pointer' }}>Rise Edge Only</label>
							<br />
							<input type="checkbox" id="highlightRisingEdge" onClick={() => { this.highlightRiseEdgeOnly($('#highlightRisingEdge').prop('checked')); }} /> <label htmlFor="highlightRisingEdge" style={{ cursor: 'pointer' }}>Highlight Rise Edge</label>
							<br />
							<input type="checkbox" id="showOnlyInOutCheckbox" defaultChecked onClick={() => { this.showInputAndOutputOnly($('#showOnlyInOutCheckbox').prop('checked')); }} /> <label htmlFor="showOnlyInOutCheckbox" style={{ cursor: 'pointer' }}>Show In/Out Only</label>
							<hr />
							{
								this.vcd != null && myConfig.user_email != '' &&
								<div>
									<button className="btn btn-light" onClick={this.saveVCD} style={{ marginBottom: '5px' }}><FontAwesomeIcon icon={solidIcons.faSave} /> Save</button>
								</div>
							}
							<div>
								{this.savedVCDButtons}
							</div>
						</div>
						<div className='col-lg-10 col-md-10 col-sm-12'>
							<div id="savedVCDDate" style={{ position: 'absolute', right: '10px', top: '10px' }}></div>
							<div className="tabset">
								<input type="radio" name="tabset" id="tab1" aria-controls="testbench" defaultChecked />
								<label htmlFor="tab1">Test Bench</label>
								<input type="radio" name="tabset" id="tab2" aria-controls="vcd" />
								<label htmlFor="tab2">VCD</label>
								<input type="radio" name="tabset" id="tab3" aria-controls="yosys" />
								<label htmlFor="tab3">Yosys</label>
								<input type="radio" name="tabset" id="tab4" aria-controls="inout" />
								<label htmlFor="tab4">In/Out</label>
								<input type="radio" name="tabset" id="tab5" aria-controls="graph" />
								<label htmlFor="tab5">Graph</label>

								<div className="tab-panels">
									<section id="testbench" className="tab-panel">
										<div style={{ position: 'absolute', left: 0, top: 100, width: '100%', height: '100%', textAlign: 'center', display: this.isLoading }}>
											<img src={require('./image/loading.svg')} />
										</div>
										<table id="wireTable">
											<thead>
												<tr>
													{this.wireTH_Top}
												</tr>
												<tr>
													{this.wireTH}
												</tr>
											</thead>
											<tbody>
												{this.dataTR}
											</tbody>
										</table>
									</section>
									<section id="vcd" className="tab-panel">
										<pre>{this.vcdString}</pre>
									</section>
									<section id="yosys" className="tab-panel">
										<div id="yosysSVGDiv" dangerouslySetInnerHTML={{ __html: this.yosys }} />
									</section>
									<section id="inout" className="tab-panel">
										<pre>{JSON.stringify(this.inout, undefined, 2)}</pre>
									</section>
									<section id="graph" className="tab-panel">
										<button className="btn btn-primary" onClick={() => { this.loadGoJS(this.topModuleJson); }}>Reload</button>
										<div id="myDiagramDiv" style={{ width: 'calc(100vw - 480px)', height: 'calc(100vh - 280px)' }}></div>
									</section>
								</div>
							</div>
						</div>
					</div>
					<div className='row' style={{ display: 'none' }}>
						<div className='col'>
							<pre id="vcdDiv"></pre>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

